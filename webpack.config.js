const webpack = require('webpack');

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
        {
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                        '@babel/react',
                        {
                            'plugins': ['@babel/plugin-proposal-class-properties']
                        }
                    ]
                },
            },
            test: /\.js$/,
            exclude: /node_modules/
        },
        {
            use: ['style-loader', 'css-loader'],
            test: /\.css$/,
        },
        {
            use: ['json-loader'],
            test: /\.json$/
        }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist/js',
    publicPath: '/',
    filename: 'app.js'
  },
  devServer: {
    contentBase: './dist',
    hot: true
  },
  mode: 'development'
};